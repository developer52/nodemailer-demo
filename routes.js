'use strict';
const Joi = require("joi");
const test = require("./handler/pages");
module.exports = [
    {
        method : 'GET',
        path : '/',
        config : {
            handler : test.root,
            auth: {
                strategy: 'session',
                mode: 'try'
            },
            plugins: {
                'hapi-auth-cookie': {
                    redirectTo: false
                }
            }
        }

    },
    {
        method : 'POST',
        path : '/send',
        handler : test.send,
        config : {
            validate : {
                payload : {
                    mail : Joi.string().email().required(),
                    subject:Joi.string().allow(''),
                    // body:Joi.string().required(),
                   
                }
            }
        }
    },
   
    
    {
        method: 'GET',
        path: '/assets/{param*}',
        handler: {
            directory: {
                path: './assets'
            }
        }
    }
];
