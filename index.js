'use strict';
const Hapi = require("hapi");
const server = new Hapi.Server();
const Path = require('path');

server.connection(
    {
        port : process.env.PORT || 6100,
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'public')
            }
        }
    }
    
);

var mysql      = require('mysql');

var db = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'dev_panic_app'
});

// server.bind({db : db});
server.bind(
    {
        db : db,
        // apiBaseUrl: 'http://localhost:4000/api',
        // webBaseUrl: 'http://localhost:4000'
    }
);

module.exports = db;
const options = {
    // ops: {
    //     interval: '10000'
    // },
    reporters: {
        myConsoleReporter: [ {
            module: 'good-console',
        }, 'stdout']
    }
};
server.ext('onPreResponse', (request, reply) => {

    if (request.response.isBoom) {
        const err = request.response;
        const errName = err.output.payload.error;
        const statusCode = err.output.payload.statusCode;

        return reply.view('ErrorPage.html', {
            statusCode: statusCode,
            errName: errName
        })
        .code(statusCode);
    }

    reply.continue();
});
server.register([
    {
        register: require('good'),
        options
    },
    {
        register: require('inert')
    },
    {
        register : require('vision'),
        options
    },
    {
        register : require('hapi-auth-cookie'),
    },
    {
        register: require('hapi-server-session'),cookie: {isSecure: false}
    }
    
], (err) => {

    if (err) {
        return console.error(err);
    }
    const cache = server.cache({ segment: 'sessions', expiresIn: 3 * 24 * 60 * 60 * 1000 });
    
    server.app.cache = cache;
    
    server.auth.strategy('session', 'cookie',  {
        password: 'password-should-be-32-characters',
        cookie: 'panic',
        redirectTo: '/login',
        isSecure: false,
        validateFunc: function (request, session, callback) {
            
            cache.get(session.uid, (err, cached) => {
                if (err) {
                    return callback(err, false);
                }

                if (!cached) {
                    return callback(null, false);
                }
                return callback(null, true, cached.account);
            });
        }
    });
    server.route(require("./routes"));
    server.views({
         engines: {
            'html': {
                module : require('handlebars'),
                compileMode: 'sync'
            }
        },
        compileMode: 'async',
        relativeTo: __dirname,
        path: './public',
        // layoutPath: './public/pages/layouts',
        helpersPath : './public/helpers',
        isCached: false
    });
    server.start(() => {
        console.info(`Server started at ${ server.info.uri }`);
    });

});