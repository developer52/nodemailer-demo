'use strict';
var db = require('../../index');
const crypto = require('crypto');
var algorithm = 'aes256'; // or any other algorithm supported by OpenSSL
var key = '2453612598';
var text = 'paasdating';
exports.generateToken = function() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

exports.mysql_real_escape_string = function (val) {
    val = val.toString().replace(/[\0\n\r\b\t\\'"\x1a]/g, function (s) {
    switch (s) {
      case "\0":
        return "\\0";
      case "\n":
        return "\\n";
      case "\r":
        return "\\r";
      case "\b":
        return "\\b";
      case "\t":
        return "\\t";
      case "\x1a":
        return "\\Z";
      case "'":
        return "''";
      case '"':
        return '""';
      default:
        return "\\" + s;
    }
  });

  return val;
}

exports.Validate_user = function(id , accessTkn , callback){
	var is_status = 'false';
	var isExistRec = "SELECT COUNT(*) AS findRow FROM users WHERE id = ? AND token = ?";
	var para = [];
	console.log(accessTkn);
	para.push(id,accessTkn);
	db.query(isExistRec , para , (err , isExistRec_result) => {
		
		if(err){
			callback(err,null);
		}
		if(isExistRec_result){
			if(isExistRec_result[0].findRow == 1 || isExistRec_result[0].findRow == '1'){
				is_status = 'true';
				callback(null,is_status);
			}else{
				callback(null,is_status);
			}
		}else{
			callback(null,is_status);
		}
		
		
	});
}

exports.distance = function(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    if (unit=="K") { dist = dist * 1.609344 }
    if (unit=="N") { dist = dist * 0.8684 }
    return dist;
  }

exports.getDistanceFromLatLonInKm = function(lat1,lon1,lat2,lon2) {
	function deg2rad(deg) {
		return deg * (Math.PI/180)
	}
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
  }
exports.ConvertDateTo_mysql_DateTime = function(dt){
  var date;
  date = new Date(dt);
  date = date.getUTCFullYear() + '-' +
    ('00' + (date.getUTCMonth()+1)).slice(-2) + '-' +
    ('00' + date.getUTCDate()).slice(-2) + ' ' + 
    ('00' + date.getUTCHours()).slice(-2) + ':' + 
    ('00' + date.getUTCMinutes()).slice(-2) + ':' + 
    ('00' + date.getUTCSeconds()).slice(-2);
  return date;
}
exports.ConvertDateTo_mysql_DateTime_v2 = function(dt){
  var date;
  date = new Date(dt);
  return (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
}
exports.ConvertMysqlTimeTo_JsTime = function(dt){
  var date;
  date = new Date(dt);
  console.log(date);
  var am_pm = date.getHours() >= 12 ? "PM" : "AM";
  return ('00' + date.getHours()).slice(-2) + ':' + ('00' + date.getMinutes()).slice(-2) + ':' +  ('00' + date.getSeconds()).slice(-2) + ' ' + am_pm;
}
exports.ConvertMysqlTimeTo_JsTime_v2 = function(dt){
  var jstime = dt.toString();
  var slots = jstime.split(":");
  var am_pm = slots[0] >= 12 ? "PM" : "AM";
  var time = slots[0] + ":" +slots[1] +":"+ slots[2] + " " +am_pm;
  return time;
}
exports.DecryptPass = function(val){
  var decipher = crypto.createDecipher(algorithm, key);
  var decrypted = decipher.update(val, 'hex', 'utf8') + decipher.final('utf8');
  return decrypted;
}
exports.EnryptPass = function(val){
  var cipher = crypto.createCipher(algorithm, key);  
  var encrypted = cipher.update(val, 'utf8', 'hex') + cipher.final('hex');
  return encrypted;
}

exports.GetTIme_zone = function(city, offset) {
    // create Date object for current location
    var d = new Date();

    // convert to msec
    // subtract local time zone offset
    // get UTC time in msec
    var utc = d.getTime() - (d.getTimezoneOffset() * 60000);

    // create new Date object for different city
    // using supplied offset
    var nd = new Date(utc + (3600000*offset));

    // return time as a string
    return "The local time for city"+ city +" is "+ nd.toLocaleString();
}
exports.getDateTime = function() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

}

exports.Convert_01_00_AM_PM_to_mysqlTime = function(time){
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if(AMPM == "PM" && hours<12) hours = hours+12;
    if(AMPM == "AM" && hours==12) hours = hours-12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if(hours<10) sHours = "0" + sHours;
    if(minutes<10) sMinutes = "0" + sMinutes;
    var sQlTime = sHours + ":" + sMinutes + ":" + "00";
    return sQlTime;
}
exports.search =  function(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].name === nameKey) {
            return myArray[i];
        }
    }
}

exports.removeDuplicates =  function(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}

exports.dedupe = function(arr) {
  return arr.reduce(function (p, c) {
    var key = [c.uid, c.deviceid].join('|');
    if (p.temp.indexOf(key) === -1) {
      p.out.push(c);
      p.temp.push(key);
    }
    return p;
  }, { temp: [], out: [] }).out;
}